package main

import (
	"log"
	"net/http"
	"os"

	_ "github.com/joho/godotenv/autoload"

	"bitbucket.org/fadialkatut/weather-app/be/pkg/rest"
	"bitbucket.org/fadialkatut/weather-app/be/pkg/weather"
)

func main() {
	var weatherAPIKey, weatherURL string
	if weatherAPIKey = os.Getenv("WEATHER_API_KEY"); weatherAPIKey == "" {
		log.Fatal("WEATHER_API_KEY is missing")
	}
	if weatherURL = os.Getenv("WEATHER_API_BASE_URL"); weatherURL == "" {
		log.Fatal("WEATHER_API_BASE_URL is missing")
	}

	c := &http.Client{}
	w := weather.NewService(c, weatherAPIKey, weatherURL)

	s := rest.NewServer(&w)
	http.Handle("/weather", s.WeatherHandler())
	log.Fatal(http.ListenAndServe(":8080", nil))

}
