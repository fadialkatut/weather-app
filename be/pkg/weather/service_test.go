package weather_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"bitbucket.org/fadialkatut/weather-app/be/pkg/weather"
)

func TestShorten(t *testing.T) {
	type args struct {
		Cities []string
	}
	type fields struct {
		ResponseJSON string
	}
	type want struct {
		Error    string
		Response []weather.Forcast
	}

	testTable := map[string]struct {
		Args   args
		Fields fields
		Want   want
	}{
		"unsupported city": {
			Args: args{Cities: []string{"CovidFreeCity"}},
			Want: want{Response: []weather.Forcast{{City: "CovidFreeCity", Error: "requested city is not supported at the moment."}}},
		},
		"malformed response": {
			Args:   args{Cities: []string{"Sydney"}},
			Fields: fields{ResponseJSON: `not a valid json`},
			Want:   want{Error: "error while unmarshalling the response : invalid character 'o' in literal null (expecting 'u')"},
		},
		"success": {
			Args:   args{Cities: []string{"Sydney", "Melbourne", "Adelaide"}},
			Fields: fields{ResponseJSON: `{"cnt":3,"list":[{"coord":{"lon":151.21,"lat":-33.87},"sys":{"country":"AU","timezone":36000,"sunrise":1590785437,"sunset":1590821707},"weather":[{"id":801,"main":"Clouds","description":"few clouds","icon":"02d"}],"main":{"temp":10.8,"feels_like":8.25,"temp_min":9.44,"temp_max":12,"pressure":1028,"humidity":93},"visibility":10000,"wind":{"speed":3.6,"deg":330},"clouds":{"all":20},"dt":1590790145,"id":2147714,"name":"Sydney"},{"coord":{"lon":144.96,"lat":-37.81},"sys":{"country":"AU","timezone":36000,"sunrise":1590787522,"sunset":1590822622},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"main":{"temp":10.65,"feels_like":5.03,"temp_min":10,"temp_max":11.11,"pressure":1022,"humidity":81},"visibility":10000,"wind":{"speed":7.2,"deg":360},"clouds":{"all":0},"dt":1590790145,"id":2158177,"name":"Melbourne"},{"coord":{"lon":138.6,"lat":-34.93},"sys":{"country":"AU","timezone":34200,"sunrise":1590788616,"sunset":1590824581},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"main":{"temp":13.95,"feels_like":6.36,"temp_min":12.78,"temp_max":15,"pressure":1017,"humidity":41},"visibility":10000,"wind":{"speed":8.2,"deg":20},"clouds":{"all":0},"dt":1590790144,"id":2078025,"name":"Adelaide"}]}`},
			Want:   want{Response: []weather.Forcast{{City: "Sydney", Details: "Clouds - few clouds, temp (9.44/12.00)"}, {City: "Melbourne", Details: "Clear - clear sky, temp (10.00/11.11)"}, {City: "Adelaide", Details: "Clear - clear sky, temp (12.78/15.00)"}}},
		},
	}
	for name, tt := range testTable {
		tt := tt
		t.Run(name, func(t *testing.T) {
			testServer := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
				_, _ = res.Write([]byte(tt.Fields.ResponseJSON))
			}))
			w := weather.NewService(&http.Client{}, "api-key", testServer.URL)
			res, err := w.Forcast(context.Background(), tt.Args.Cities)
			if tt.Want.Error != "" {
				assert.EqualError(t, err, tt.Want.Error, "Error")
				return
			}
			require.NoError(t, err, "Error")
			assert.Equal(t, tt.Want.Response, res, "Response")
		})
	}
}
