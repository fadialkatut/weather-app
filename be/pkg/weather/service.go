package weather

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

const errorMsg = "error fetching weather"

// Service manages weather calls
type Service struct {
	client        *http.Client
	citiesMapping map[string]string
	weatherAPIKey string
	weatherURL    string
}

// Forcast details
type Forcast struct {
	City    string `json:"city"`
	Details string `json:"forcast"`
	Error   string `json:"error"`
}

type weather struct {
	Description string `json:"description"`
	Main        string `json:"main"`
}

type main struct {
	TempMax float64 `json:"temp_max"`
	TempMin float64 `json:"temp_min"`
}

type item struct {
	Main    main      `json:"main"`
	Name    string    `json:"name"`
	Weather []weather `json:"weather"`
}

type response struct {
	List []item `json:"list"`
}

// NewService creates a new weather service instance
func NewService(c *http.Client, key, url string) Service {
	return Service{citiesMapping: map[string]string{"Sydney": "2147714", "Melbourne": "2158177", "Adelaide": "2078025"}, client: c, weatherAPIKey: key, weatherURL: url}
}

//Forcast gets the cities forcast from openweathermap
func (s *Service) Forcast(ctx context.Context, cities []string) ([]Forcast, error) {
	f := make([]Forcast, 0)
	ids := make([]string, 0)
	for _, c := range cities {
		id, ok := s.citiesMapping[c]
		if !ok {
			f = append(f, Forcast{City: c, Error: "requested city is not supported at the moment."})
			continue
		}
		ids = append(ids, id)
	}
	if len(ids) == 0 {
		return f, nil
	}
	j := fmt.Sprintf(`%s/data/2.5/group?id=%s&units=metric&appid=%s`, s.weatherURL, strings.Join(ids, ","), s.weatherAPIKey)
	req, err := http.NewRequest("GET", j, nil)
	req.Header.Set("Content-Type", "application/json")
	req = req.WithContext(ctx)
	resp, err := s.client.Do(req)
	if err != nil {
		fmt.Printf("error executing request for: %s - %s\n", ids, err.Error())
		return nil, errors.Wrap(err, "error executing request")
	}
	defer resp.Body.Close()
	r, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("error while reading the response for: %s - %s\n", ids, err.Error())
		return nil, errors.Wrap(err, "error while reading the response")
	}
	var res response
	if err := json.Unmarshal(r, &res); err != nil {
		fmt.Printf("error while unmarshalling the response for: %s - %s\n", ids, err.Error())
		return nil, errors.Wrap(err, "error while unmarshalling the response ")
	}
	for _, li := range res.List {
		f = append(f, Forcast{City: li.Name, Details: fmt.Sprintf("%s - %s, temp (%.2f/%.2f)", li.Weather[0].Main, li.Weather[0].Description, li.Main.TempMin, li.Main.TempMax)})
	}
	return f, nil
}
