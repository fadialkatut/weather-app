package rest

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/fadialkatut/weather-app/be/pkg/weather"
)

type weatherService interface {
	Forcast(ctx context.Context, cities []string) ([]weather.Forcast, error)
}

// Server exposes the rest api
type Server struct {
	w weatherService
}

// NewServer creates a new rest api instance
func NewServer(ws weatherService) *Server {
	return &Server{w: ws}
}

// WeatherHandler fetches the weather for the given cities
func (s *Server) WeatherHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		if r.Method != "GET" {
			http.NotFound(w, r)
			return
		}
		citiesParam := r.URL.Query().Get("cities")
		if citiesParam == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Missing cities parameter"))
			return
		}
		var cities []string
		if err := json.Unmarshal([]byte(citiesParam), &cities); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Error parsing cities parameter"))
			return
		}
		f, err := s.w.Forcast(r.Context(), cities)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Error fetching cities forcast, please try again later"))
			fmt.Println(err.Error())
			return
		}
		b, err := json.Marshal(f)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Error fetching cities forcast, please try again later"))
			fmt.Println(err.Error())
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(b)
	})
}
