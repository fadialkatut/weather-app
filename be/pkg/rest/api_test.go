package rest_test

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"bitbucket.org/fadialkatut/weather-app/be/pkg/rest"
	"bitbucket.org/fadialkatut/weather-app/be/pkg/weather"
)

func TestServer(t *testing.T) {
	type args struct {
		Method string
		Path   string
	}
	type fields struct {
		MockExpectations func(ctx context.Context, w *weatherService)
	}
	type want struct {
		Response string
		Status   int
	}

	testTable := []struct {
		Name   string
		Args   args
		Fields fields
		Want   want
	}{
		{
			Name: "post request",
			Args: args{
				Method: "POST",
				Path:   "/weather",
			},
			Fields: fields{MockExpectations: func(ctx context.Context, w *weatherService) {}},
			Want:   want{Response: "404 page not found\n", Status: 404},
		},
		{
			Name: "no cities provided",
			Args: args{
				Method: "GET",
				Path:   "/weather",
			},
			Fields: fields{MockExpectations: func(ctx context.Context, w *weatherService) {}},
			Want:   want{Response: "Missing cities parameter", Status: 400},
		},
		{
			Name: "malformed cities provided",
			Args: args{
				Method: "GET",
				Path:   "/weather?cities=not array",
			},
			Fields: fields{MockExpectations: func(ctx context.Context, w *weatherService) {}},
			Want:   want{Response: "Error parsing cities parameter", Status: 400},
		},
		{
			Name: "error executing request",
			Args: args{
				Method: "GET",
				Path:   "/weather?cities=[%22Melbourne%22]",
			},
			Fields: fields{MockExpectations: func(ctx context.Context, w *weatherService) {
				w.On("Forcast", ctx, []string{"Melbourne"}).Return([]weather.Forcast{}, errors.New("some error"))
			}},
			Want: want{Response: "Error fetching cities forcast, please try again later", Status: 500},
		},
		{
			Name: "success",
			Args: args{
				Method: "GET",
				Path:   "/weather?cities=[%22Melbourne%22]",
			},
			Fields: fields{MockExpectations: func(ctx context.Context, w *weatherService) {
				w.On("Forcast", ctx, []string{"Melbourne"}).Return([]weather.Forcast{{City: "Melbourne", Details: "Clear - clear sky, temp (6.67/11.00)"}}, nil)
			}},
			Want: want{Response: "[{\"city\":\"Melbourne\",\"forcast\":\"Clear - clear sky, temp (6.67/11.00)\",\"error\":\"\"}]", Status: 200},
		},
	}
	for _, tt := range testTable {
		tt := tt
		t.Run(tt.Name, func(t *testing.T) {
			var w weatherService
			svc := rest.NewServer(&w)
			req, err := http.NewRequest(tt.Args.Method, "http://exampleserver.com.au"+tt.Args.Path, bytes.NewBuffer([]byte("")))
			res := httptest.NewRecorder()
			assert.NoError(t, err, "Error creating request")
			tt.Fields.MockExpectations(req.Context(), &w)
			req.Host = "example.com"
			svc.WeatherHandler().ServeHTTP(res, req)
			assert.Equal(t, tt.Want.Status, res.Code, "Status code")
			html, err := ioutil.ReadAll(res.Body)
			require.NoError(t, err, "Error reading response")
			assert.Equal(t, tt.Want.Response, string(html), "Response body")
			mock.AssertExpectationsForObjects(t, &w)
		})
	}
}

type weatherService struct {
	mock.Mock
}

func (w *weatherService) Forcast(ctx context.Context, cities []string) ([]weather.Forcast, error) {
	args := w.Called(ctx, cities)
	return args.Get(0).([]weather.Forcast), args.Error(1)
}
