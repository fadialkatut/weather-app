import React from 'react';
import Checkbox from './Checkbox';

const items = [
    'Sydney',
    'Melbourne',
    'Adelaide',
    'CovidFreeCity'
];


class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = { cities: [], result: [] };
        this.selectedCheckboxes = new Set();
    }

    toggleCheckbox = label => {
        if (this.selectedCheckboxes.has(label)) {
            this.selectedCheckboxes.delete(label);
        } else {
            this.selectedCheckboxes.add(label);
        }
    }

    createCheckbox = label => (
        <Checkbox
            label={label}
            handleCheckboxChange={this.toggleCheckbox}
            key={label}
        />
    )

    createCheckboxes = () => (
        items.map(this.createCheckbox)
    )

    createResult = res => (
        <div>
            <p><strong>{res.city} </strong></p> <p>{res.forcast}</p> <p>{res.error}</p>
        </div>
    )

    createResults = () => (
        this.state.result.map(this.createResult)
    )

    mySubmitHandler = (event) => {
        this.state = { cities: [] };
        event.preventDefault();
        for (const checkbox of this.selectedCheckboxes) {
            this.state.cities.push(checkbox)
        }
        fetch('http://localhost:8080/weather?cities=' + JSON.stringify(this.state.cities))
            .then(res => res.json())
            .then((data) => {
                this.setState({ result: data })
            })
            .catch(alert)
    }

    render() {
        return (
            <form onSubmit={this.mySubmitHandler}>
                <h1>Weather app</h1>
                <p>Choose your city:</p>
                {this.createCheckboxes()}
                <input
                    type='submit'
                />
                <br />
                {this.createResults()}
            </form>
        );
    }
}

export default Form;