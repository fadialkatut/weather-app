## Overview
This project is a solution for Outcomex Coding Exercise. The focus is to create a pragmatic, testable and maintainable code. 

## Environment

The following environment should be available and configured properly on the host machine in order to run and test the solution:

    - Git
    - Docker - am using engine version 19.03.8 & compose 1.25.5
    - Go version go1.12.1 darwin/amd64 (if not using docker)
    - NPM version 6.14.4
    - Node version 14.3.0
    - Your favourite cmd client 
    
## Setup

Clone the repository

```
$ git clone https://bitbucket.org/fadialkatut/weather-app.git
```

### Folder structure

The repo contains 2 projects, the golang backend (be) and the reacjs forntend (fe). the backend assumes that you will be cloning the repo inside `.../gocode/src/bitbucket.org/fadialkatut/` folder structure in order for imports to work properly.

### Dependencies

The go app depends on two libs:
    - github.com/stretchr/testify for unit tests.
    - github.com/joho/godotenv for anaging environment variables.

### Config

You will need to provide your own `openweathermap` api key. to do so. create an environment file by copying the example one:

```
$ cd be
$ cp .env.example .env
```

and update the values inside it.

### Using Docker

start docker inside each project (be & fe)

```
$ docker-compose up
```

navigate to [http://127.0.0.1:3000/](http://127.0.0.1:3000/)

### Without Docker

#### Backend

Download dependencies by running

```
$ make depend
```

Run the app by executing

```
$ make run
```

#### Frontend

Download dependencies by running

```
$ nom install
```

Run the app by executing

```
$ npm start
```

navigate to [http://127.0.0.1:3000/](http://127.0.0.1:3000/)

## Approach

The solution is done using go and reacjs as per the code challenge instructions.

### Room for enhancement?

Due to time restriction the app is missing some of the features that would make it better. In a reallife scenario I would include/use:
	
    - Retry faild api requests.
    - Add caching layer to reduce the number of requests to the 3rd party api.
    - Add auth layer for the backend endpoint.
    - CI/CD capabilities.
    - Add linting capabilities to produce more readable and cleaner code.
    - Observability - Logging, tracing and metrics.
    - Manage dependencies using Gopkg.toml and dep ensure
    - Enhance request validation.
    - Configure the http client properly.
    - Add unit tests for frontend.
    - Use config file for frontend to point to backend url.
    - Deploy with Kubernetes for scaling and monitoring.

### Testing 

Unite test cases are used to automate the testing on the bacjkend and can be run using the following command:

```
$ make test
```
